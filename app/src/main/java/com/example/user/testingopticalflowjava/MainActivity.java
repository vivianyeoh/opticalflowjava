package com.example.user.testingopticalflowjava;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.video.Video;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/* The following code is extracted from main project, Myotongue, to test the accuracy of the optical
flow detection.
User will scan their face first, after their face and mouth is found, user press start to continue. The face will
stored in faceStartPoint, faceEndPoint, mouthStartPoint, mouthEndPoint. These data will store the face
location and will not change after start button is pressed. User then swap their tongue and manually type
the direction of their tongue. The row of button table is movement done by user and column of button table
is result output by system. With the data, the accuracy of the system will be computed and displayed later.
After user quit the camera will be disabled. When user returned, they will have to rescan their face
to continue. 
 */

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    private static final String TAG = "StartActivity";
    public static final String MyPREFERENCES = "OpticalFlowJava";
    private String prevPosture = "";
    private int ll = 0, lr = 0, lu = 0, ld = 0, nl = 0, rl = 0, rr = 0, ru = 0, rd = 0, nr = 0;
    private int ul = 0, ur = 0, uu = 0, ud = 0, nu = 0, dl = 0, dr = 0, du = 0, dd = 0, nd = 0;
    private int ml = 0, mr = 0, mu = 0, md = 0;//not moving
    private int count, prevPostureCount = 0;
    private boolean isStarted = false, shouldDelete;

    //user interface
    private Spinner spinner;
    private SharedPreferences sharedpreferences;
    private ImageButton btnInfo;
    private Button btnReset, btnResume, btnFinish, btnAgain, btnDelete, btnStart;
    private Button btnLL, btnLR, btnLU, btnLD, btnRL, btnRR, btnRU, btnRD, btnUL, btnUR, btnUU, btnUD;
    private Button btnDL, btnDR, btnDU, btnDD, btnnl, btnnr, btnnu, btnnd, btnLMove, btnRMove, btnStartCamera, btnUMove, btnDMove;
    private TextView tvResult, tvll, tvlr, tvlu, tvld, tvrl, tvrr, tvru, tvrd, tvul, tvur, tvuu, tvud, tvdl, tvdr, tvdu, tvdd, tvCount, tvAccuracy;
    private TextView tvpDown, tvpLeft, tvpUp, tvpRight, tvDown, tvLeft, tvUp, tvRight, tvnl, tvnr, tvnu, tvnd, tvLMove, tvRMove, tvUMove, tvDMove, tvLevel;
    private LinearLayout viewScore, viewStart;

    //optical flow
    private MatOfPoint2f prevFeatures, nextFeatures;
    private MatOfPoint features;
    private MatOfByte status;
    private MatOfFloat err;
    private Mat mRgba, mGray, mPrevGray;
    private CameraBridgeViewBase mOpenCvCameraView;
    private CascadeClassifier faceCascade, mouthCascade;
    private int absoluteMouthSize, absoluteFaceSize;
    private Point faceStartPoint = new Point(), faceEndPoint = new Point();
    private Point mouthStartPoint = new Point(), mouthEndPoint = new Point();
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        //OpenCV manager to help our app communicate with android phone to make OpenCV work
        //Once OpenCV library is loaded, you may want to perform some actions. For example,
        // displaying a success or failure message.
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    try {
                        // load mouth cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.haarcascade_mcs_mouth);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeMouthFile = new File(cascadeDir, "haarcascade_mcs_mouth.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeMouthFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();
                        mouthCascade = new CascadeClassifier();
                        mouthCascade.load(mCascadeMouthFile.getAbsolutePath());
                        cascadeDir.delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load mouth cascade. Exception thrown: " + e);
                    }
                    try {
                        // load face cascade file from application resources
                        InputStream isFace = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDirFace = getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeFaceFile = new File(cascadeDirFace, "lbpcascade_frontalface.xml");
                        FileOutputStream osFace = new FileOutputStream(mCascadeFaceFile);

                        byte[] bufferFace = new byte[4096];
                        int bytesReadFace;
                        while ((bytesReadFace = isFace.read(bufferFace)) != -1) {
                            osFace.write(bufferFace, 0, bytesReadFace);
                        }
                        isFace.close();
                        osFace.close();
                        faceCascade = new CascadeClassifier();
                        faceCascade.load(mCascadeFaceFile.getAbsolutePath());
                        cascadeDirFace.delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load face cascade. Exception thrown: " + e);
                    }
//                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //screen configuration
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        //check permission
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
            }
        }
        //initialize ui
        initializeInterface();
        setSpinner();
        setButtons();
    }

    /*initialize all ui elements*/
    private void initializeInterface() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        spinner = findViewById(R.id.spinner);
        viewScore = findViewById(R.id.viewScore);
        viewStart = findViewById(R.id.viewStart);

        tvLevel = findViewById(R.id.tvLevel);
        tvResult = findViewById(R.id.tvResult);
        tvCount = findViewById(R.id.tvCount);
        tvAccuracy = findViewById(R.id.tvAccuracy);

        tvll = findViewById(R.id.tvll);
        tvlr = findViewById(R.id.tvlr);
        tvlu = findViewById(R.id.tvlu);
        tvld = findViewById(R.id.tvld);

        tvrl = findViewById(R.id.tvrl);
        tvrr = findViewById(R.id.tvrr);
        tvru = findViewById(R.id.tvru);
        tvrd = findViewById(R.id.tvrd);

        tvul = findViewById(R.id.tvul);
        tvur = findViewById(R.id.tvur);
        tvuu = findViewById(R.id.tvuu);
        tvud = findViewById(R.id.tvud);

        tvdl = findViewById(R.id.tvdl);
        tvdr = findViewById(R.id.tvdr);
        tvdu = findViewById(R.id.tvdu);
        tvdd = findViewById(R.id.tvdd);

        tvnl = findViewById(R.id.tvnl);
        tvnr = findViewById(R.id.tvnr);
        tvnu = findViewById(R.id.tvnu);
        tvnd = findViewById(R.id.tvnd);

        tvLMove = findViewById(R.id.tvLMove);
        tvRMove = findViewById(R.id.tvRMove);
        tvUMove = findViewById(R.id.tvUMove);
        tvDMove = findViewById(R.id.tvDMove);

        tvpDown = findViewById(R.id.tvpDown);
        tvpLeft = findViewById(R.id.tvpLeft);
        tvpRight = findViewById(R.id.tvpRight);
        tvpUp = findViewById(R.id.tvpUP);

        tvDown = findViewById(R.id.tvDown);
        tvLeft = findViewById(R.id.tvLeft);
        tvRight = findViewById(R.id.tvRight);
        tvUp = findViewById(R.id.tvUp);

        btnFinish = findViewById(R.id.btnFinish);
        btnAgain = findViewById(R.id.btnAgain);
        btnDelete = findViewById(R.id.btnDelete);
        btnResume = findViewById(R.id.btnResume);
        btnReset = findViewById(R.id.btnReset);
        btnStart = findViewById(R.id.btnStart);
        btnStartCamera = findViewById(R.id.btnStartCamera);
        btnInfo = findViewById(R.id.btnInfo);

        btnLL = findViewById(R.id.btnLL);
        btnLR = findViewById(R.id.btnLR);
        btnLU = findViewById(R.id.btnLU);
        btnLD = findViewById(R.id.btnLD);
        btnnl = findViewById(R.id.btnnl);
        btnLMove = findViewById(R.id.btnLMove);

        btnRL = findViewById(R.id.btnRL);
        btnRR = findViewById(R.id.btnRR);
        btnRU = findViewById(R.id.btnRU);
        btnRD = findViewById(R.id.btnRD);
        btnnr = findViewById(R.id.btnnr);
        btnRMove = findViewById(R.id.btnRMove);

        btnUL = findViewById(R.id.btnUL);
        btnUU = findViewById(R.id.btnUU);
        btnUD = findViewById(R.id.btnUD);
        btnUR = findViewById(R.id.btnUR);
        btnnu = findViewById(R.id.btnnu);
        btnUMove = findViewById(R.id.btnUMove);

        btnDL = findViewById(R.id.btnDL);
        btnDR = findViewById(R.id.btnDR);
        btnDU = findViewById(R.id.btnDU);
        btnDD = findViewById(R.id.btnDD);
        btnnd = findViewById(R.id.btnnd);
        btnDMove = findViewById(R.id.btnDMove);

        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.show_camera_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(MainActivity.this);

        setTextViews();
        setScoreButtons();
        reset();
    }

    /*reset ui elements*/
    private void reset() {
        ll = lr = lu = ld = 0;
        rl = rr = ru = rd = 0;
        ul = ur = uu = ud = 0;
        dl = dr = du = dd = 0;
        ml = mr = mu = md = 0;
        nl = nr = nu = nd = 0;//not detected

        tvResult.setText("Waiting");
        count = 0;
        tvCount.setText("Count: " + count);
        btnLL.setText(ll + "");
        btnLR.setText(lr + "");
        btnLU.setText(lu + "");
        btnLD.setText(ld + "");
        btnRL.setText(rl + "");
        btnRR.setText(rr + "");
        btnRU.setText(ru + "");
        btnRD.setText(rd + "");
        btnUL.setText(ul + "");
        btnUR.setText(ur + "");
        btnUU.setText(uu + "");
        btnUD.setText(ud + "");

        btnDL.setText(dl + "");
        btnDR.setText(dr + "");
        btnDD.setText(dd + "");
        btnDU.setText(du + "");

        btnnl.setText(nl + "");
        btnnr.setText(nr + "");
        btnnu.setText(nu + "");
        btnnd.setText(nd + "");

        btnLMove.setText(ml + "");
        btnRMove.setText(mr + "");
        btnUMove.setText(mu + "");
        btnDMove.setText(md + "");

        tvDown.setText("Down: 0");
        tvLeft.setText("Left: 0");
        tvRight.setText("Right: 0");
        tvUp.setText("Up: 0");

    }

    /*set normal buttons function*/
    public void setButtons() {
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    shouldDelete = !shouldDelete;
                    if (shouldDelete)
                        btnDelete.setBackgroundColor(Color.RED);
                    else
                        btnDelete.setBackgroundColor(getColor(R.color.Orange500));
                }
            }
        });

        btnResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    tvResult.setText("waiting");
                    if (mOpenCvCameraView != null)
                        mOpenCvCameraView.enableView();
                }
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    reset();
                }
            }
        });
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    viewScore.setVisibility(View.VISIBLE);
                    mouthStartPoint = new Point();
                    mouthEndPoint = new Point();
                    faceStartPoint = new Point();
                    faceEndPoint = new Point();
                    setTextViews();
                    if (mOpenCvCameraView != null) {
                        mOpenCvCameraView.disableView();
                        mOpenCvCameraView.setVisibility(View.INVISIBLE);
                    }
                    isStarted = false;
                    tvResult.setText("Waiting");
                    btnStart.setEnabled(false);
                    btnStart.setText("Press Start Camera to start");
                }
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    new CustDialogBox(MainActivity.this, "Info", "Move your tongue after a " +
                            "bounding box has shown around your face on the screen. " +
                            "It means the camera has detected your face and mouth area. " +
                            "\n\nFor the table in the right " +
                            "panel, table column is result outputted by system and table row is actual position " +
                            "that user has moved. If user has moved their tongue correctly, the camera will be " +
                            "paused and result will be outputted and displayed at the bottom left corner " +
                            "by system. User will have to press the button corresponding to the result. " +
                            "For example, if user has moved their tongue to right and system has outputted left, " +
                            "user should press button on right row and left column. This action should be " +
                            "repeated at least 10 times for each position for more accurate data. After user has " +
                            "completed inserting value, press finish button to show result." +
                            "\n\n To delete a wrong value, press delete and click on the button that you have " +
                            "pressed wrongly. Multiple selection is allowed. \n\n To reset value, press reset " +
                            "button or the value will be added until the application is closed. ");
                }
            }
        });
        btnStartCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStarted = false;
                btnStart.setText("Wait until orange box appear around your face");
                if (mOpenCvCameraView != null) {
                    mOpenCvCameraView.setVisibility(View.VISIBLE);
                    mOpenCvCameraView.enableView();
                }
            }
        });
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewStart.setVisibility(View.GONE);
                isStarted = true;
            }
        });

        btnAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewScore.setVisibility(View.GONE);
                viewStart.setVisibility(View.VISIBLE);
                isStarted = false;//should be false
            }
        });
    }

    /*set up spinner*/
    public void setSpinner() {
        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        categories.add("Hard");
        categories.add("Medium");
        categories.add("Easy");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        int spinnerPosition = sharedpreferences.getInt("level", 1);
        spinner.setSelection(spinnerPosition);

        // Spinner click listener
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                //store level chosen by user into user preference
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("level", position);
                editor.apply();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //auto generate
            }
        });
    }

    /*set score button functions*/
    private void setScoreButtons() {
        //if delete button is pressed, the value will decrease on click, else, it will increase
        btnLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ll--;
                        count--;
                    } else {
                        ll++;
                        count++;
                    }
                    btnLL.setText(ll + "");
                    tvCount.setText("Count: " + count);
                    tvLeft.setText("Left: " + (ld + lr + lu + ll + ml + nl));
                }
            }
        });
        btnLR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        lr--;
                        count--;
                    } else {
                        lr++;
                        count++;
                    }
                    tvCount.setText("Count: " + count);
                    btnLR.setText(lr + "");
                    tvLeft.setText("Left: " + (ld + lr + lu + ll + ml + nl));
                }
            }
        });
        btnLU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        lu--;
                        count--;
                    } else {
                        lu++;
                        count++;
                    }
                    btnLU.setText(lu + "");
                    tvCount.setText("Count: " + count);
                    tvLeft.setText("Left: " + (ld + lr + lu + ll + ml + nl));
                }
            }
        });
        btnLD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ld--;
                        count--;
                    } else {
                        ld++;
                        count++;
                    }
                    btnLD.setText(ld + "");
                    tvCount.setText("Count: " + count);
                    tvLeft.setText("Left: " + (ld + lr + lu + ll + ml + nl));
                }
            }
        });
        btnLMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ml--;
                        count--;
                    } else {
                        ml++;
                        count++;
                    }
                    btnLMove.setText(ml + "");
                    tvCount.setText("Count: " + count);
                    tvLeft.setText("Left: " + (ld + lr + lu + ll + ml + nl));
                }
            }
        });

        btnnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        nl--;
                        count--;
                    } else {
                        nl++;
                        count++;
                    }
                    btnnl.setText(nl + "");
                    tvCount.setText("Count: " + count);
                    tvLeft.setText("Left: " + (ld + lr + lu + ll + ml + nl));
                }
            }
        });

        btnRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        rl--;
                        count--;
                    } else {
                        rl++;
                        count++;
                    }
                    btnRL.setText(rl + "");
                    tvCount.setText("Count: " + count);
                    tvRight.setText("Right: " + (rd + rr + ru + rl + mr + nr));
                }
            }
        });
        btnRR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        rr--;
                        count--;
                    } else {
                        rr++;
                        count++;
                    }
                    btnRR.setText(rr + "");
                    tvCount.setText("Count: " + count);
                    tvRight.setText("Right: " + (rd + rr + ru + rl + mr + nr));
                }
            }
        });
        btnRU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ru--;
                        count--;
                    } else {
                        ru++;
                        count++;
                    }
                    btnRU.setText(ru + "");
                    tvCount.setText("Count: " + count);
                    tvRight.setText("Right: " + (rd + rr + ru + rl + mr + nr));
                }
            }
        });
        btnRD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        rd--;
                        count--;
                    } else {
                        rd++;
                        count++;
                    }
                    btnRD.setText(rd + "");
                    tvCount.setText("Count: " + count);
                    tvRight.setText("Right: " + (rd + rr + ru + rl + mr + nr));
                }
            }
        });
        btnRMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        mr--;
                        count--;
                    } else {
                        mr++;
                        count++;
                    }
                    btnRMove.setText(mr + "");
                    tvCount.setText("Count: " + count);
                    tvRight.setText("Right: " + (rd + rr + ru + rl + mr + nr));
                }
            }
        });

        btnnr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        nr--;
                        count--;
                    } else {
                        nr++;
                        count++;
                    }
                    btnnr.setText(nr + "");
                    tvCount.setText("Count: " + count);
                    tvRight.setText("Right: " + (rd + rr + ru + rl + mr + nr));
                }
            }
        });

        btnUL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ul--;
                        count--;
                    } else {
                        ul++;
                        count++;
                    }
                    btnUL.setText(ul + "");
                    tvCount.setText("Count: " + count);
                    tvUp.setText("Up: " + (ud + ur + uu + ul + mu + nu));
                }
            }
        });
        btnUR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ur--;
                        count--;
                    } else {
                        ur++;
                        count++;
                    }
                    btnUR.setText(ur + "");
                    tvCount.setText("Count: " + count);
                    tvUp.setText("Up: " + (ud + ur + uu + ul + mu + nu));
                }
            }
        });
        btnUU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        uu--;
                        count--;
                    } else {
                        uu++;
                        count++;
                    }
                    btnUU.setText(uu + "");
                    tvCount.setText("Count: " + count);
                    tvUp.setText("Up: " + (ud + ur + uu + ul + mu + nu));
                }
            }
        });
        btnUD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        ud--;
                        count--;
                    } else {
                        ud++;
                        count++;
                    }
                    btnUD.setText(ud + "");
                    tvCount.setText("Count: " + count);
                    tvUp.setText("Up: " + (ud + ur + uu + ul + mu + nu));
                }
            }
        });
        btnUMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        mu--;
                        count--;
                    } else {
                        mu++;
                        count++;
                    }
                    btnUMove.setText(mu + "");
                    tvCount.setText("Count: " + count);
                    tvUp.setText("Up: " + (ud + ur + uu + ul + mu + nu));
                }
            }
        });

        btnnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        nu--;
                        count--;
                    } else {
                        nu++;
                        count++;
                    }
                    btnnu.setText(nu + "");
                    tvCount.setText("Count: " + count);
                    tvUp.setText("Up: " + (ud + ur + uu + ul + mu + nu));
                }
            }
        });

        btnDL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        dl--;
                        count--;
                    } else {
                        dl++;
                        count++;
                    }
                    btnDL.setText(dl + "");
                    tvCount.setText("Count: " + count);
                    tvDown.setText("Down: " + (dd + dr + du + dl + md + nd));
                }
            }
        });
        btnDR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        dr--;
                        count--;
                    } else {
                        dr++;
                        count++;
                    }
                    btnDR.setText(dr + "");
                    tvCount.setText("Count: " + count);
                    tvDown.setText("Down: " + (dd + dr + du + dl + md + nd));
                }
            }
        });
        btnDU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        du--;
                        count--;
                    } else {
                        du++;
                        count++;
                    }
                    btnDU.setText(du + "");
                    tvCount.setText("Count: " + count);
                    tvDown.setText("Down: " + (dd + dr + du + dl + md + nd));
                }
            }
        });
        btnDD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        dd--;
                        count--;
                    } else {
                        dd++;
                        count++;
                    }
                    btnDD.setText(dd + "");
                    tvCount.setText("Count: " + count);
                    tvDown.setText("Down: " + (dd + dr + du + dl + md + nd));
                }
            }
        });
        btnDMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        md--;
                        count--;
                    } else {
                        md++;
                        count++;
                    }
                    btnDMove.setText(md + "");
                    tvCount.setText("Count: " + count);
                    tvDown.setText("Down: " + (dd + dr + du + dl + md + nd));
                }
            }
        });

        btnnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarted) {
                    if (shouldDelete) {
                        nd--;
                        count--;
                    } else {
                        nd++;
                        count++;
                    }
                    btnnd.setText(nd + "");
                    tvCount.setText("Count: " + count);
                    tvDown.setText("Down: " + (dd + dr + du + dl + md + nd));
                }
            }
        });
    }

    /*set data of textviews*/
    private void setTextViews() {
        int spinnerPosition = sharedpreferences.getInt("level", 1);
        if (spinnerPosition == 0)
            tvLevel.setText("Hard");
        if (spinnerPosition == 1)
            tvLevel.setText("Medium");
        if (spinnerPosition == 2)
            tvLevel.setText("Easy");

        tvAccuracy.setText("Accuracy: " + calAccuracy() + "%");

        tvll.setText(ll + "/" + (ld + lr + lu + ll + ml + nl));
        tvlr.setText(lr + "/" + (ld + lr + lu + ll + ml + nl));
        tvlu.setText(lu + "/" + (ld + lr + lu + ll + ml + nl));
        tvld.setText(ld + "/" + (ld + lr + lu + ll + ml + nl));
        tvnl.setText(nl + "/" + (ld + lr + lu + ll + ml + nl));
        tvLMove.setText(ml + "/" + (ld + lr + lu + ll + ml + nl));
        tvpLeft.setText(calLeftAccuracy() + "%");

        tvrl.setText(rl + "/" + (rd + rr + ru + rl + mr + nr));
        tvrr.setText(rr + "/" + (rd + rr + ru + rl + mr + nr));
        tvru.setText(ru + "/" + (rd + rr + ru + rl + mr + nr));
        tvrd.setText(rd + "/" + (rd + rr + ru + rl + mr + nr));
        tvnr.setText(nr + "/" + (rd + rr + ru + rl + mr + nr));
        tvRMove.setText(mr + "/" + (rd + rr + ru + rl + mr + nr));
        tvpRight.setText(calRightAccuracy() + "%");

        tvul.setText(ul + "/" + (ud + ur + uu + ul + mu + nu));
        tvur.setText(ur + "/" + (ud + ur + uu + ul + mu + nu));
        tvuu.setText(uu + "/" + (ud + ur + uu + ul + mu + nu));
        tvud.setText(ud + "/" + (ud + ur + uu + ul + mu + nu));
        tvnu.setText(nu + "/" + (ud + ur + uu + ul + mu + nu));
        tvUMove.setText(mu + "/" + (ud + ur + uu + ul + mu + nu));
        tvpUp.setText(calUpAccuracy() + "%");

        tvdl.setText(dl + "/" + (dd + dr + du + dl + md + nd));
        tvdr.setText(dr + "/" + (dd + dr + du + dl + md + nd));
        tvdu.setText(du + "/" + (dd + dr + du + dl + md + nd));
        tvdd.setText(dd + "/" + (dd + dr + du + dl + md + nd));
        tvnd.setText(nd + "/" + (dd + dr + du + dl + md + nd));
        tvDMove.setText(md + "/" + (dd + dr + du + dl + md + nd));
        tvpDown.setText(calDownAccuracy() + "%");

    }

    /*to calculate total accuracy*/
    private int calAccuracy() {
        if (count > 0) return (int) ((double) (ll + rr + dd + uu) / (double) count * 100);
        else return 0;
    }

    /*to calculate accuracy of left position*/
    private int calLeftAccuracy() {
        if ((ld + lr + lu + ll + nl + ml) > 0)
            return (int) ((double) ll / (double) (ld + lr + lu + ll + nl + ml) * 100);
        else return 0;
    }

    /*to calculate accuracy of right position*/
    private int calRightAccuracy() {
        if ((rd + rr + ru + rl + nr + mr) > 0)
            return (int) ((double) rr / (double) (rd + rr + ru + rl + nr + mr) * 100);
        else return 0;
    }

    /*to calculate accuracy of up position*/
    private int calUpAccuracy() {
        if ((ud + ur + uu + ul + nu + mu) > 0)
            return (int) ((double) uu / (double) (ud + ur + uu + ul + nu + mu) * 100);
        else return 0;
    }

    /*to calculate accuracy of down position*/
    private int calDownAccuracy() {
        if ((dd + dr + du + dl + nd + md) > 0)
            return (int) ((double) dd / (double) (dd + dr + du + dl + nd + md) * 100);
        else return 0;
    }

    /*android lifecycle on resume*/
    @Override
    public void onResume() {
        super.onResume();
        //set up camera
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, MainActivity.this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        //to reset the face and mouth location
        isStarted = false;
        mouthStartPoint = new Point();
        mouthEndPoint = new Point();
        faceStartPoint = new Point();
        faceEndPoint = new Point();
        viewStart.setVisibility(View.VISIBLE);
    }

    /*android lifecycle on pause*/
    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
            mOpenCvCameraView.setVisibility(View.INVISIBLE);
        }
//        mouthStartPoint = new Point();
//        mouthEndPoint = new Point();
//        faceStartPoint = new Point();
//        faceEndPoint = new Point();
//        isStarted = false;
    }

    /*android lifecycle on destroy*/
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
            mOpenCvCameraView.setVisibility(View.INVISIBLE);
        }
    }

    /* function from CameraBridgeViewBase.CvCameraViewListener2
   * set up camera variables*/
    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
        resetVars();
        //face detection - compute minimum face size
        if (this.absoluteFaceSize == 0) {
            int h = mGray.rows();
            if (Math.round(h * 0.4f) > 0) {
                this.absoluteFaceSize = Math.round(h * 0.4f);
            }
        }
        //mouth detection - compute minimum mouth size (20% of the frame height, in our case)
        if (this.absoluteMouthSize == 0) {
            int h = absoluteFaceSize;
            if (Math.round(h * 0.4f) > 0) {
                this.absoluteMouthSize = Math.round(h * 0.4f);
            }
        }
    }

    /* function from CameraBridgeViewBase.CvCameraViewListener2
    * release data*/
    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
    }

    /* function from CameraBridgeViewBase.CvCameraViewListener2
  * things to do when camera is enabled*/
    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        //make mirror view
        Core.flip(mRgba, mRgba, 1);
        Core.flip(mGray, mGray, 1);

        //set fixed face rectangle that the user rectangle must stay inside this rectangle
        Point fixedFaceStart = new Point(0, 0);
        Point fixedFaceEnd = new Point(mOpenCvCameraView.getWidth(), mOpenCvCameraView.getHeight());
        //draw rectangle
        Imgproc.rectangle(mRgba, fixedFaceStart, fixedFaceEnd, new Scalar(0, 0, 255), 6);

        //if the user face is not detected, detect user face
        if (!isStarted) {
            // detect face with opencv function
            MatOfRect faces = new MatOfRect();
            this.faceCascade.detectMultiScale(mGray, faces, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                    new Size(this.absoluteFaceSize, this.absoluteFaceSize), new Size());

            final Rect[] faceArray = faces.toArray();
            if (faceArray.length > 0) {
                //use the first value in the array because it is more likely to be a correct one
                final double faceStartx = faceArray[0].tl().x;
                final double faceStarty = faceArray[0].tl().y;
                final double faceEndx = faceArray[0].br().x;
                final double faceEndy = faceArray[0].br().y;
                double faceCenterx = (faceStartx + faceEndx) / 2;
                double faceCentery = (faceStarty + faceEndy) / 2;
                // detect mouths
                MatOfRect mouths = new MatOfRect();
                this.mouthCascade.detectMultiScale(mGray, mouths, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                        new Size(this.absoluteMouthSize, this.absoluteMouthSize), new Size());

                final Rect[] mouthsArray = mouths.toArray();
                if (mouthsArray.length > 0) {
                    final double mouthStartx = mouthsArray[0].tl().x;
                    final double mouthStarty = mouthsArray[0].tl().y;
                    final double mouthEndx = mouthsArray[0].br().x;
                    final double mouthEndy = mouthsArray[0].br().y;
                    //position of mouth must be at the lower half of their face
                    if (mouthStartx > faceStartx
                            && mouthStartx < faceCenterx
                            && mouthStarty > faceCentery
                            && faceEndx > mouthEndx
                            && faceEndy > mouthEndy) {
                        //calculate distance between user face bounding box and fixed face bounding box
                        //and add constraint
                        //user's face must be inside the box or not too far from the bounding box
                        if (euclideanDist(fixedFaceStart, faceArray[0].tl()) < 10
                                | euclideanDist(fixedFaceEnd, faceArray[0].br()) < 10
                                | (faceStartx > fixedFaceStart.x
                                && faceStarty > fixedFaceStart.y
                                && fixedFaceEnd.x > faceEndx
                                && fixedFaceEnd.y > faceEndy)) {
                            //if the above conditions are met, set user face point
                            mouthStartPoint = mouthsArray[0].tl();
                            mouthEndPoint = mouthsArray[0].br();
                            faceStartPoint = faceArray[0].tl();
                            faceEndPoint = faceArray[0].br();
                            //enables start button to start the game
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnStart.setEnabled(true);
                                    btnStart.setText("Start");
                                }
                            });
                            //draw user face bounding box
                            Imgproc.rectangle(mRgba, faceArray[0].tl(), faceArray[0].br(), new Scalar(232, 114, 11), 3);
                        } else {
                            //if user face did not meet condition, warn user about it in start button
                            mouthStartPoint = new Point();
                            mouthEndPoint = new Point();
                            faceStartPoint = new Point();
                            faceEndPoint = new Point();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    btnStart.setEnabled(false);
                                    btnStart.setText("Put your face in that box!");
                                }
                            });
                        }
                    }
                }
            } else {
                //if user face did not meet condition, warn user about it in start button
                mouthStartPoint = new Point();
                mouthEndPoint = new Point();
                faceStartPoint = new Point();
                faceEndPoint = new Point();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnStart.setEnabled(false);
                        btnStart.setText("Put your face in that box!");
                    }
                });
            }
        }
        //run optical flow around mouth area with reserved face and mouth area
        if (features.toArray().length == 0) {
            //set up feature points for the whole camera
            int rowStep = 12, colStep = 25;
            int nRows = mGray.rows() / rowStep, nCols = mGray.cols() / colStep;
            Point points[] = new Point[nRows * nCols];
            for (int i = 0; i < nRows; i++) {
                for (int j = 0; j < nCols; j++) {
                    points[i * nCols + j] = new Point(j * colStep, i * rowStep);
                }
            }
            features.fromArray(points);
            prevFeatures.fromList(features.toList());
            mPrevGray = mGray.clone();
        }
        nextFeatures.fromArray(prevFeatures.toArray());

        //draw fixed rectangle of user bounding box
        Imgproc.rectangle(mRgba, faceStartPoint, faceEndPoint, new Scalar(232, 114, 11), 3);
        //calculate optical flow and store that data in nextFeatures
        Video.calcOpticalFlowPyrLK(mPrevGray, mGray, prevFeatures, nextFeatures, status, err);
        mPrevGray = mGray.clone();

        List<Point> prevList = features.toList();
        List<Point> nextList = nextFeatures.toList();
        Scalar color = new Scalar(255, 0, 0);

        int direction[] = new int[4];
        int movedPoint = 0, totalPoint = 0;
        for (int i = 0; i < prevList.size(); i++) {
            double x1 = prevList.get(i).x;
            double y1 = prevList.get(i).y;
            double x2 = nextList.get(i).x;
            double y2 = nextList.get(i).y;
            //returned data must stay inside fixed mouth bounding box
            if (x1 > 0 && y1 > 0 && x2 > 0 && y2 > 0)
                if (x1 > mouthStartPoint.x && y1 > mouthStartPoint.y)
                    if (x2 > mouthStartPoint.x && y2 > mouthStartPoint.y)
                        if (x1 < mouthEndPoint.x && y1 < mouthEndPoint.y)
                            if (x2 < mouthEndPoint.x && y2 < mouthEndPoint.y) {
                                totalPoint++;
                                //to filter noise, the distance between prev point and next point must more than 5
                                if (euclideanDist(prevList.get(i), nextList.get(i)) > 3) {
                                    //calculate number of points that have met the condition
                                    movedPoint++;
                                    if (euclideanDist(prevList.get(i), nextList.get(i)) > 7) {
                                        //set result with the function below
                                        direction = findDirection(prevList.get(i), nextList.get(i), direction);
                                    }
                                }
                                //draw the prev and next points
                                Imgproc.line(mRgba, prevList.get(i), nextList.get(i), color, 5);
                            }
        }
        //store final direction in dir
        final String dir = displayDir(direction, sharedpreferences.getInt("level", 1), movedPoint, totalPoint);
        //display result in the camera screen
        Imgproc.putText(mRgba, "up:" + direction[0] + " right:" + direction[1] + " down:" + direction[2] + " left:" + direction[3],
                new Point(0, 100), Core.FONT_HERSHEY_SIMPLEX,// front face
                1, new Scalar(255, 0, 0), 2);
        //if game has been started
        if (isStarted) {
            //user tongue will go to one direction in a few frame, therefore, noise will be created.
            //to filter noise, set timer to eliminate same direction data
            //next position cannot same as previous one in small time period to reduce noise
            if (!dir.equals(prevPosture)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (dir) {
                            //switch the position
                            //for that case, set text view and prevPosture with the result
                            //pause the camera view to let user see the actual position clearly
                            case "left":
                                tvResult.setText("Left");
                                prevPosture = "left";
                                if (mOpenCvCameraView != null) {
                                    mOpenCvCameraView.disableView();
                                }
                                break;
                            case "right":
                                tvResult.setText("Right");
                                prevPosture = "right";
                                if (mOpenCvCameraView != null) {
                                    mOpenCvCameraView.disableView();
                                }
                                break;
                            case "up":
                                tvResult.setText("Up");
                                prevPosture = "up";
                                if (mOpenCvCameraView != null) {
                                    mOpenCvCameraView.disableView();
                                }
                                break;
                            case "down":
                                tvResult.setText("Down");
                                prevPosture = "down";
                                if (mOpenCvCameraView != null) {
                                    mOpenCvCameraView.disableView();
                                }
                                break;
                        }
                    }
                });
            } else {
                prevPostureCount++;
                if (prevPostureCount > 5) {
                    prevPosture = "";
                    prevPostureCount = 0;
                }
            }
        }
        return mRgba;
    }

    /* to calculate euclidean distance of two point*/
    private double euclideanDist(Point p, Point q) {
        double ycoord = Math.abs(p.y - q.y);
        double xcoord = Math.abs(p.x - q.x);
        return Math.sqrt((ycoord) * (ycoord) + (xcoord) * (xcoord));
    }

    /*return the position that most points in ttlPoint going to
   /*the maximum value should more than a threshold to reduce noise
   /*data is returned in a string datatype */
    private String displayDir(int[] dir, int level, int movedPoint, int totalPoint) {
        if (movedPoint > totalPoint * 90 / 100) {
            //if most point in movedPoint going toward same direction,
            //it might not be user's tongue moving
            return "Move your tongue only!";
        }

        int max = -1, index = 0, threshold;
        for (int i = 0; i < dir.length; i++) {
            if (dir[i] > max) {
                max = dir[i];
                index = i;
            }
        }
        //calculate threshold
        if (level == 0) {
            threshold = (int) ((double) movedPoint * 60 / 100);
        } else if (level == 1) {
            threshold = (int) ((double) movedPoint * 50 / 100);
        } else {
            threshold = (int) ((double) movedPoint * 40 / 100);
        }

        //compute result
        if (max > threshold && movedPoint >= 3) {
            if (index == 0)
                return "up";
            else if (index == 1)
                return "right";
            else if (index == 2)
                return "down";
            else if (index == 3)
                return "left";
            else
                return "error";
        } else {
            return "Extend your tongue fully!";
        }
    }

    /* angle between two points is calculated with atan function
   * the angle will be used to find direction from one point to another
   * the result is stored in dir array
   * code referred from https://www.mathopenref.com/triggraphtan.html*/
    private int[] findDirection(Point prev, Point next, int[] dir) {
        double diffx = next.x - prev.x;
        double diffy = next.y - prev.y;
        double angle = Math.toDegrees(Math.atan(diffy / diffx));
        if (diffx > 0) {
            if (angle > 45) dir[2]++;//return "down";
            else if (angle > -45) dir[1]++;//return "right";
            else dir[0]++;//return "up";
        } else {
            angle += 180;
            if (angle < 135) dir[2]++;//return "down";
            else if (angle < 225) dir[3]++;//return "left";
            else dir[0]++;//return "up";
        }
        return dir;
    }

    /*reset mat variables*/
    private void resetVars() {
        mPrevGray = new Mat(mGray.rows(), mGray.cols(), CvType.CV_8UC1);
        features = new MatOfPoint();
        prevFeatures = new MatOfPoint2f();
        nextFeatures = new MatOfPoint2f();
        status = new MatOfByte();
        err = new MatOfFloat();
    }
}